function inicioAnimacao(){
	$('#interatividade').fadeIn(500);

	InterfaceController.btnReiniciarCallback = function(){		
		InterfaceController.closeAbas();
		zeraProgresso();
		intro();				
	};
	InterfaceController.btnFasesCallback = function(){
		feedAlerta();
	};

	$('#intro .grafismo').attr('src','application/css/img/grafismo_'+$base_grafismo+'.png');
	TweenMax.fromTo('#intro .grafismo',2.5,{scale:0},{scale:1,ease:Elastic.easeOut, delay: .5});

	silencioEfeitos();
	intro();
}

InterfaceController.onOpenAbas = function(){
	pauseAudio('trilha');
	pauseAudio('efects');
	pauseAudio('locucao');
}

InterfaceController.onCloseAbas = function(){
	playPausedAudio('trilha');
	playPausedAudio('efects');
	playPausedAudio('locucao');
}

function zeraProgresso(){
	for(var i in Texto){
		Texto[i].finalizou = false;
	}
}

function zeraAudios(){
	var p;
	for( p in Audios)
		Audios[p].tocou = false;
}

function replayTrilha(){
	if( !Audios['trilha'].tocou ){
		Audios['trilha'].tocou = true;
		showCredito('.credito',Audios['trilha'].credito);
	}
	playAudio('trilha','trilha',replayTrilha);
}

function tocaEfeitos(fx,callback){
	if( !Audios[fx].tocou ){
		Audios[fx].tocou = true;
		showCredito('.credito',Audios[fx].credito);
	}
	playAudio('efects',fx,function(){
		silencioEfeitos();
		if(typeof(callback) == 'function') callback();
	});
}

function silencioEfeitos(){ playAudio('efects','load',function(){}); }

function tocarLocucao(){
	playAudio('locucao',$locucao,silencioLocucao);
}

function repetirLocucao(){
	$('.btn-locucao').unbind('click').bind('click',function(){
		playAudio('locucao',$locucao,silencioLocucao);
	});	
}

function silencioLocucao(){
	playAudio('locucao','load');
}

var dentroFase = false;
function intro(){
	atvAtual = 0;
	pontuacaoFases = [];
	acertos = 0;
	concluiuTudo = false;
	dentroFase = false;
	
	var p;
	for(p in Texto)
		Texto[p].concluiu = false;

	zeraAudios();
	replayTrilha();

	$locucao = 'abertura';
	playAudio('locucao', $locucao,silencioLocucao);
	repetirLocucao();

	$('#parabens, #escolhaFases').fadeOut(500);
	$('#interatividade #atividade, #interatividade .respostas').hide();

	$('.clicksFase span').attr('data-icon','');

	$('#intro, #intro .botao').fadeIn(500);
	$('#intro #bloco2 p').html(Textos.intro);
	$('#intro blockquote .iniciar').unbind('click').bind('click',function(){
		silencioLocucao();
		$('#intro').fadeOut(500);
		$('.credito').addClass('oneLine');
		tocaEfeitos('click');
		verificaFase();
		dentroFase = true;
	});

	for (var i = 0; i < Texto.length; i++) {
		pontuacaoFases.push(0);
	}

	//$('#btnFases').parent().hide();
	$('#words').tinyscrollbar({invertscroll: true});
	boxScroll = $('#words').data("plugin_tinyscrollbar");

	grafismo = new Motio($('#intro .grafismo')[0], { fps: 18, frames: 60, startPaused: true });
	grafismo.toStart(true);
	setTimeout( grafismo.toEnd, 500);

}

function verificaFase(){
	var btnsFase = '';
	var finalizouCheck = '';

	$('#escolhaFases').hide();
	$('#escolhaFases blockquote').html('');

	if (Texto.length >= 2) {
		for (var i = 0; i < Texto.length; i++) {
			if (Texto[i].finalizou){
				finalizouCheck = '<img src="application/css/img/check.png"/>'
			}else{
				finalizouCheck = ''
			}
			btnsFase += '<button class="clicksFase" id="clkFase_'+i+'"><p>'+finalizouCheck+'<strong>'+Texto[i].titulo+'</strong><br/>'+Texto[i].subtitulo+'</p><span data-icon="vazio"></span></button>';
		}

		$('#escolhaFases blockquote').attr('data-twocol',Texto.length>4).html(btnsFase);
		$('#escolhaFases').fadeIn(500);

		$('.clicksFase').unbind('click').bind('click',function(){
			var _idFases = parseInt($(this).attr('id').replace('clkFase_',''));

			$('#escolhaFases').fadeOut(500);
			atvAtual = _idFases;

			atividade();
		});

		if (Texto.length >= 5) {
			$('#escolhaFases blockquote, #escolhaFases button').addClass('faseMaiorCinco');
		}
	}else{
		atividade();
	}
}

var $scroll,
	boxScroll,
	grafismo,
	pontuacaoFases = [],
	acertos = 0;

function atividade(){
	$('#atividade').fadeIn(500);

	var i,
	overview_width = 0,
	$viewport = $("#atividade nav .viewport"),
	$overview = $viewport.children(".overview"),
	$titulo = $("#atividade .title-bar h1"),
	$subtitulo = $("#atividade .title-bar p"),
	$texto = $("#atividade article blockquote"),
	$currentWords = Words[atvAtual],
	$buttonWords = [];
	shuf = toShuffle( $currentWords );

	$lock = $('#atividade .lock');
	$lock.css('height','61px');
	count = 0;

	$("#atividade article").attr('class', ( (Texto[atvAtual].alinhamento) ? Texto[atvAtual].alinhamento : '' ) );
	
	$titulo.html( Texto[atvAtual].titulo );
	$texto.html( Texto[atvAtual].texto );
	$subtitulo.html( Texto[atvAtual].subtitulo );	
	$texto.children("p").each(function(i,el){ 
		var t = $(el).html();
		t = t.replace(/\[\[/g,"<span class='drop'><b>");
		t = t.replace(/\]\]/g,"</b></span>");
		$(el).html(t);
	});

	$overview.html('');
	for( i = 0; i < shuf.length; i++)
		$overview.append("<button class='drag' data-status='on' data-id='"+shuf[i]+"'>"+$currentWords[shuf[i]]+"</button>");

	$overview.children("button").each(function(i,el){ 
		overview_width += $(el).outerWidth(true);
		$buttonWords.push($(el));
	});		
	$overview.css({left:0, width:overview_width});

	$("#atividade article").tinyscrollbar({invertscroll: true});

	$('#atividade .drag')
		.draggable({
			cursor: "move",
			appendTo: '#atividade',
			helper: 'clone',
			cancel: '[data-status=off]',
			revert: function(droped){ if(droped === false){ revert( $(this) ) } },
			start: function( event, ui ) { 
				Functions.dndStartFunction(ui);
				$(ui.helper).css({zIndex:9999});	
				$(this).attr('data-status','off');
				tocaEfeitos('drag');
			},
			drag: function( event, ui ) {},
			stop: function( event, ui ) { $(ui.helper).css({zIndex:1}); }
		});

	$('#atividade .drop')
		.droppable({
			tolerance: "pointer",
			drop: function( event, ui ) {
				var $drag = $(ui.helper).clone(),
					$drop = $(this),
					status = '',
					audio = '';
				
				if( ui.position.top <= 470 ){
					if( $drop.children('.drag').size() > 0 ){
						revert( $('#atividade .drag[data-id='+$drag.data('id')+']') );
						return false;
					}else{
						$drop.addClass('fill');
						if( $drag.html().toString().toLowerCase() == $drop.children('b').html().toString().toLowerCase() ){
							status = 'toCorrect';
							audio = 'correct';
							count ++;
						}else{
							status = 'toError';
							audio = 'error';
							TweenMax.delayedCall(2,function(){
								revert( $('#atividade .drag[data-id='+$drag.data('id')+']') );
								$drop.children('.drag').remove();
								$drop.removeClass('fill');
							})
						}
						$drag
							.attr('style','margin-left:-'+($(ui.helper).outerWidth() / 2)+'px')
							.attr('class','drag')
							.attr('data-status',status)
							.appendTo( $drop );
						tocaEfeitos('drop',function(){
							tocaEfeitos(audio,function(){
								$lock
									.css('height','61px')
									.hide();
								
								liberaClick = true;	
							});
							$drag.attr('data-status',audio);
							TweenMax.delayedCall(1.5,verificarLigacoes);
						});
						liberaClick = false;	
						$lock
							.css('height','126px')
							.show();

						return true;
					}
				}else{
					revert( $('#atividade .drag[data-id='+$drag.data('id')+']') );
					return false;
				}
			}
		});

	$('.lock').hide();

	function revert( $ui ){
		$ui.attr('style','').attr('data-status','on');
	}
	
	function verificarLigacoes( ){
		if( count == $('#atividade article p span').length ){
			tocaEfeitos('final');
			var checkfinal = verificaFinaliza();
			finaliza(checkfinal);
		}
	}


	var liberaClick = true,
		proximoBtn = 0,
		anteriorBtn = 0,
		centerSize = 0;
	
	if ($(window).width() <= 290){
		centerSize = 0;
	}else if($(window).width() <= 320 && $(window).width() > 290){
		centerSize = 15;
	}else if($(window).width() <= 375 && $(window).width() > 320){
		centerSize = 55;
	}else if($(window).width() <= 425 && $(window).width() > 375){
		centerSize = 75;
	}
	$('#atividade nav .anterior').hide().unbind('keydown tap').bind('keydown tap', function(e){
		e.stopPropagation();
		if (e.keyCode === 13 || e.type === 'tap') {
			if(liberaClick){
				var _l = parseInt($overview.css('left'));
				_l = - $buttonWords[anteriorBtn].position().left + centerSize;
				voltaVetor();
				if( _l >= 0 ){
					_l = 0;
					$('#atividade nav .anterior').fadeOut(500);
				}
				$('#atividade nav .proximo').fadeIn(500);
				animaOverview(_l);
			}
		}

	});
	$('#atividade nav .proximo').show().unbind('keydown tap').bind('keydown tap', function(e){
		e.stopPropagation();
		if (e.keyCode === 13 || e.type === 'tap') {
			if(liberaClick){
				var _l = parseInt($overview.css('left')),
					_limit = ($overview.width() - $viewport.width());
				moveVetor();
				_l = - $buttonWords[proximoBtn].position().left + centerSize;
				if( _l <= -_limit ){
					_l = -_limit;
					$('#atividade nav .proximo').fadeOut(500);
				}
				$('#atividade nav .anterior').fadeIn(500);
				animaOverview(_l);
			}
		}
	});
	function animaOverview(left){
		liberaClick = false;
		tocaEfeitos('click');
		TweenMax.to( $overview, 1, { left: left, ease: Power4.easeInOut, onComplete: function(){ liberaClick = true; } } );
	}
	function moveVetor(){
		if(anteriorBtn == 0 && proximoBtn == 0){ 
			proximoBtn = 1;
		}else{
			anteriorBtn++;
			proximoBtn++;
		}
	}
	function voltaVetor(){
		anteriorBtn--;
		proximoBtn--;
	}

	recursoVerResposta = true;
	$('#atividade .resposta').show().unbind('keydown tap').bind('keydown tap', function(e){
		e.stopPropagation();
		if (e.keyCode === 13 || e.type === 'tap') {
			if(recursoVerResposta){
				tocaEfeitos('click');
				recursoVerResposta = false;
				$('#atividade .drop').not('.fill').each(function(i,el){
					var needle = $(el).children('b').html(),
						$option = $('#atividade nav .drag:contains('+needle+')')
						$optionClone = $option.clone();
					
					$(el).addClass('fill');
					$option.attr('data-status','off');
					$optionClone
						.attr('style','margin-left:-'+($option.outerWidth() / 2)+'px')
						.attr('class','drag')
						.attr('data-status','solved')
						.appendTo( $(el) );
				});
				$('.lock').show();
				$('#atividade .resposta').hide();
				TweenMax.delayedCall(1,function(){
					$('#atividade .reiniciar').fadeIn(500);
					$('#atividade .avancar').fadeIn(500);
				});
			}
		}
	});

	$('#atividade .reiniciar').hide().unbind('click').bind('click',function(){
		feedAlerta();
	});

	$('#atividade .avancar').hide().unbind('click').bind('click',function(){
		avancarTrapaca();
	});

	setFocus('#atividade .viewport');
}	


function feedAlerta(){
	var $texto = $('#feedConfirma article p'),
		$feedConfirma = $('#feedConfirma, #feedConfirma .botao');

	$texto.html(Textos.alert);

	$('#feedConfirma .sim').unbind('click').bind('click',function(){
		$feedConfirma.fadeOut(500);
		//$('#escolhaFases').fadeIn(500);
		//$('#btnFases').parent().hide();
		tocaEfeitos('click',verificaFase);
	});

	$('#feedConfirma .nao').unbind('click').bind('click',function(){
		$feedConfirma.fadeOut(500);
		tocaEfeitos('click');
	});

	$feedConfirma.fadeIn(500);
}

var total,
	totalFases,
	mensagemFinal = '',
	textoBotao = '',
	concluiuTudo = false;

function reiniciar(){
	InterfaceController.closeAbas();
	zeraAudios();
	zeraProgresso();
	$('.tela').fadeOut(500);
	intro();
}

function avancar(){
	$('.tela').fadeOut(50);
	verificaFase();
}

function avancarTrapaca(){
	$('.tela').fadeOut(500);
	var checkfinal = verificaFinaliza();
	tocaEfeitos('final');
	finaliza(checkfinal);
}

function verificaFinaliza(){
	Texto[atvAtual].finalizou = true;
	for(var i in Texto){
		if (Texto[i].finalizou === false){
			return false;
		}
	}
	return true;
}

function finaliza(checkfinal){
	if(checkfinal){
		mensagemFinal = Textos.msgFinalizar
		textoBotao = 'Reiniciar';
	}else{
		mensagemFinal = Textos.msgAvancar
		textoBotao = 'Avançar';
	}
	$('#parabens article p').html(mensagemFinal);
	$('#parabens article .reinit').html(textoBotao);

	TweenMax.set( '#parabens article', { transform: 'scale(0)'} );
	$('#parabens').css('display','none');
	$('#parabens').css('display','block');
	$('#parabens').fadeIn(500,function(){
		setFocus('#parabens');
		TweenMax.to( '#parabens article', 1.5, { transform: 'scale(1)', ease: Elastic.easeOut} );
	});
	
	$('#parabens article .fechar').unbind('keydown tap').bind('keydown tap', function(e){
		e.stopPropagation();
		if (e.keyCode === 13 || e.type === 'tap') {
			$('#parabens').fadeOut(500);
			tocaEfeitos('click');
			$('#atividade .resposta').fadeOut(500);
			$('#atividade .reiniciar').fadeIn(500);
			
			$lock
				.css('height','61px')
				.show();
		}
	});	

	$('.reinit').unbind('click').bind('click',function(){
		if(checkfinal){
			tocaEfeitos('click',reiniciar);
		}else{
			tocaEfeitos('click',avancar);
		}
	});
}


function inArray(needle, haystack) {
    var length = haystack.length;
    for(var i = 0; i < length; i++) {
        if(haystack[i] == needle) return true;
    }
    return false;
}
function toShuffle(arr){
	var aux = [], i, shuff;
	for(i = 0; i < arr.length; i++){
		aux.push( i );					
	}
	shuff = shuffle(aux);
	while(shuff.toString() === arr.toString()){
		shuff = shuffle(aux);
	}
	return shuff;
}
function shuffle(o){
    for(var j, x, i = o.length; i; j = Math.floor(Math.random() * i), x = o[--i], o[i] = o[j], o[j] = x);
    return o;
}