﻿Textos.creditos = '<p><b>Gerência de tecnologia:</b> Argeu Pereira da Ivenção</p>'+
	'<p><b>Coordenação executiva:</b> Ivonete Darci Lucírio Gonzaga </p>'+
	'<p><b>Coordenação de conteúdo:</b> Luciana Saito</p>'+
	'<p><b>Roteiro:</b> [nome completo do roteirista]</p>'+
	'<p><b>Edição de conteúdo:</b> [nome completo do editor]</p>'+
	'<p><b>Revisão técnica:</b> [nome completo do(s) revisor(es) técnico(s)]</p>'+
	'<p><b>Revisão de texto:</b> Ramiro Morais Torres </p>'+
	'<p><b>Coordenação de arte:</b> Eduardo Reche Bertolini</p>'+
	'<p><b>Edição de arte:</b> Diogo de Assis Macedo, Rodrigo Luis de Andrade</p>'+
	'<p><b>Assistência de arte:</b> Ana Maria Totaro Delgado</p>'+
	'<p><b>Iconografia:</b> Fabiana Manna da Silva, Renate Hartfiel</p>'+
	'<p><b>Programação:</b> Renato Frias Rocha Ibiapina</p>'+
	'<p><b>Assistência de programação:</b> Rodrigo Vieira Benfica Amancio</p>'+
	'<p><b>Assistência de produção e checagem:</b> Fabiana Aparecida Martins, Natália Lamucio Andrade, Renata Campos Michelin</p>'+
	'<p><b>Locução:</b> [nome completo do(s) locutor(es), quando houver]</p>'+
	'<p><b>Produção:</b> Daniel Palmeira dos Passos, Renato Frias Rocha Ibiapina</p>'+
	'<p>A editora realizou todos os esforços para localizar os titulares dos direitos autorais, nem sempre com resultado.<br/>'+
	'A editora reserva os direitos para o caso de comprovada titularidade.</p>'+
	'<p>Reprodução proibida. Art. 184 do Código Penal e Lei 9.610, de 19 de fevereiro de 1998.</p>'+
	'<p>Todos os direitos reservados. </p>'+
	'<p>EDITORA MODERNA<br/>'+
	'Rua Padre Adelino, 758 – Belenzinho<br/>'+
	'São Paulo, SP – Brasil – CEP 03303-904<br/>'+
	'<p>www.moderna.com.br</p>'+
	'<p>Produzido no Brasil</p>';

Textos.ajuda = '<p>Complete o texto arrastando as palavras que estão na barra inferior para as lacunas corretas.</p><p>Às vezes, para ver todas as palavras disponíveis, você precisará clicar nas setas que estarão ao lado da barra. Pode haver palavras que não se encaixam no texto e, por isso, não deverão ser utilizadas.</p><p>Sempre que você colocar as peças no local correto, elas serão destacadas em verde; se você as colocar no local errado, elas serão indicadas em vermelho e voltarão à barra inferior.</p><p>Se quiser saber a solução da atividade, clique em “Ver resposta”.</p>';

Textos.intro = 'Vamos conhecer mais sobre xxxxxxxx? Na atividade a seguir, complete os textos escolhendo as opções corretas para cada lacuna.';
Textos.alert = 'Todas as suas ações nesta fase serão perdidas. Deseja continuar?';

Textos.fases = [
	'<strong>Parabéns!</strong> Você encontrou<br/> todas as palavras.',
	'<strong>Muito bem!</strong> Você encontrou <span class="qto"></span><br/> das <span class="total"></span> palavras.',
	'Você encontrou <span class="qto"></span> das<br/> <span class="total"></span> palavras.',
	'Você encontrou <span class="qto"></span><br/> das <span class="total"></span> palavras.',
	'Você encontrou <span class="qto"></span><br/> das <span class="total"></span> palavras.',
	'Você não encontrou<br/> nenhuma palavra.'
];
Textos.final = [
	'<strong>Parabéns!</strong> Você encontrou todas as palavras de todas as fases.',
	'<strong>Muito bem!</strong> Você encontrou <span class="qto"></span><br/> das <span class="total"></span> palavras.<br/><b>Que tal tentar encontrar todas?</b>',
	'Você encontrou <span class="qto"></span> das<br/> <span class="total"></span> palavras... Mas é possível melhorar. <b>Vamos tentar de novo?</b>',
	'Você encontrou <span class="qto"></span><br/> das <span class="total"></span> palavras. <br/><b>Que tal tentar mais uma vez?</b>',
	'Você encontrou <span class="qto"></span><br/> das <span class="total"></span> palavras. <br/><b>Tente de novo!</b>',
	'Você não encontrou<br/> nenhuma palavra... <br/><b>Tente de novo!</b>'
];

Textos.msgAvancar = '<strong>PARABÉNS!</strong>Você completou corretamente todo o texto.</b>';
Textos.msgFinalizar = '<strong>PARABÉNS!</strong>Você completou corretamente todos os textos!</b>';

var Audios = {
	'trilha':  	 { credito: 'IMAGENS: KOTOFFEI/SHUTTERSTOCK;<br/>COMPOSIÇÃO: EDUARDO RECHE BERTOLINI;<br/>TRILHA SONORA: “AIRPORT LOUNGE” – KEVIN MACLEOD/INCOMPETECH', tocou: false },
	'correct':   { credito: 'EFEITO SONORO: JAVIERZUMER/FREESOUND', tocou: false },
	'error':   	 { credito: 'EFEITO SONORO: TIMGORMLY/FREESOUND', tocou: false },
	'final':  { credito: 'EFEITO SONORO: FINS/FREESOUND', tocou: false },
	'drag':    { credito: 'EFEITO SONORO: ROMULOFS/FREESOUND', tocou: false },
	'drop':    { credito: 'EFEITO SONORO: QUBODUP/FREESOUND', tocou: false },
	'click':     { credito: 'EFEITO SONORO: AGENTVIVID/FLASHKIT', tocou: false }
}

var Texto = [
	{titulo: 'Fase 1', subtitulo: 'O rio Paraguai 1', texto: '<p>As pessoas que vivem nas margens do rio Paraguai pescam e fazem plantio de mandioca, melancia, [[abóboras]] e outros alimentos.</p> <p>Mesmo com alguns problemas, os moradores da região optam por viver ali pela facilidade de acesso aos barcos, usados para vender e comprar produtos para seu sustento.</p> <p>Às vezes, as pessoas são pegas de surpresa e precisam sair de suas [[casas]] por causa da rápida subida das águas, causada pelas [[chuvas]]. Elas precisam se mudar para lugares mais altos, para  escapar da [[enchente]].</p> <p>Até 2002, não havia escolas para as famílias ribeirinhas da região. Em 2003 e 2004, foram construídas duas [[escolas]] para atender adultos e crianças. O calendário das escolas é adaptado aos períodos de [[cheias]] e de seca do Rio Paraguai.</p> <small>LIMA, A. O.; SOARES, J.B.; GRECO, J.B. et al. Romeu. <i>Métodos de laboratório aplicados<br/> à clínica técnica e interpretação</i>. 8. ed. Rio de Janeiro: Guanabara Koogan, 2001.</small>'
	, alinhamento: 'esquerda', finalizou: false},
	{titulo: 'Fase 2', subtitulo: 'O rio Paraguai 2', texto: '<p>As pessoas que vivem nas margens do rio Paraguai pescam e fazem plantio de mandioca, melancia, [[abóboras]] e outros alimentos.</p> <p>Mesmo com alguns problemas, os moradores da região optam por viver ali pela facilidade de acesso aos barcos, usados para vender e comprar produtos para seu sustento.</p> <p>Às vezes, as pessoas são pegas de surpresa e precisam sair de suas [[casas]] por causa da rápida subida das águas, causada pelas [[chuvas]]. Elas precisam se mudar para lugares mais altos, para  escapar da [[enchente]].</p> <p>Até 2002, não havia escolas para as famílias ribeirinhas da região. Em 2003 e 2004, foram construídas duas [[escolas]] para atender adultos e crianças. O calendário das escolas é adaptado aos períodos de [[cheias]] e de seca do Rio Paraguai.</p> <small>LIMA, A. O.; SOARES, J.B.; GRECO, J.B. et al. Romeu. <i>Métodos de laboratório aplicados<br/> à clínica técnica e interpretação</i>. 8. ed. Rio de Janeiro: Guanabara Koogan, 2001.</small>'
	, alinhamento: 'esquerda', finalizou: false},
	{titulo: 'Fase 3', subtitulo: 'O rio Paraguai 3', texto: '<p>As pessoas que vivem nas margens do rio Paraguai pescam e fazem plantio de mandioca, melancia, [[abóboras]] e outros alimentos.</p> <p>Mesmo com alguns problemas, os moradores da região optam por viver ali pela facilidade de acesso aos barcos, usados para vender e comprar produtos para seu sustento.</p> <p>Às vezes, as pessoas são pegas de surpresa e precisam sair de suas [[casas]] por causa da rápida subida das águas, causada pelas [[chuvas]]. Elas precisam se mudar para lugares mais altos, para  escapar da [[enchente]].</p> <p>Até 2002, não havia escolas para as famílias ribeirinhas da região. Em 2003 e 2004, foram construídas duas [[escolas]] para atender adultos e crianças. O calendário das escolas é adaptado aos períodos de [[cheias]] e de seca do Rio Paraguai.</p> <small>LIMA, A. O.; SOARES, J.B.; GRECO, J.B. et al. Romeu. <i>Métodos de laboratório aplicados<br/> à clínica técnica e interpretação</i>. 8. ed. Rio de Janeiro: Guanabara Koogan, 2001.</small>'
	, alinhamento: 'esquerda', finalizou: false},
	{titulo: 'Fase 4', subtitulo: 'O rio Paraguai 4', texto: '<p>As pessoas que vivem nas margens do rio Paraguai pescam e fazem plantio de mandioca, melancia, [[abóboras]] e outros alimentos.</p> <p>Mesmo com alguns problemas, os moradores da região optam por viver ali pela facilidade de acesso aos barcos, usados para vender e comprar produtos para seu sustento.</p> <p>Às vezes, as pessoas são pegas de surpresa e precisam sair de suas [[casas]] por causa da rápida subida das águas, causada pelas [[chuvas]]. Elas precisam se mudar para lugares mais altos, para  escapar da [[enchente]].</p> <p>Até 2002, não havia escolas para as famílias ribeirinhas da região. Em 2003 e 2004, foram construídas duas [[escolas]] para atender adultos e crianças. O calendário das escolas é adaptado aos períodos de [[cheias]] e de seca do Rio Paraguai.</p> <small>LIMA, A. O.; SOARES, J.B.; GRECO, J.B. et al. Romeu. <i>Métodos de laboratório aplicados<br/> à clínica técnica e interpretação</i>. 8. ed. Rio de Janeiro: Guanabara Koogan, 2001.</small>'
	, alinhamento: 'esquerda', finalizou: false},
]

var Words = [
	['abóboras','casas','chuvas','enchente','escolas','cheias','Santa Catarina','secas','postos'],
	['abóboras','casas','chuvas','enchente','escolas','cheias','Santa Catarina','secas','postos'],
	['abóboras','casas','chuvas','enchente','escolas','cheias','Santa Catarina','secas','postos'],
	['abóboras','casas','chuvas','enchente','escolas','cheias','Santa Catarina','secas','postos'],
];