﻿Main.titulo = 'BANCO DE PALAVRAS';
Main.tituloWithAudio = false;
Main.hasPreRun = false;
//Main.log = true;

$base_grafismo = 'geografia'; //artes, ciencias, geografia, hgc, historia, matematica, portugues

InterfaceController.mostrarBtnFases = true;
InterfaceController.mostrarBtnAjuda = true;
InterfaceController.mostrarBtnAcessibilidade = true;
InterfaceController.mostrarBtnCreditos = true;
InterfaceController.mostrarBtnReiniciar = true;

var filesJSON = { "files": [ { "source": "application/css/img/btns_nav.png", "type": "IMAGE", "size": 990 },{ "source": "application/css/img/grafismo.png", "type": "IMAGE", "size": 874270 },{ "source": "application/css/img/icones.png", "type": "IMAGE", "size": 4229 },{ "source": "application/css/img/medalha_bronze.png", "type": "IMAGE", "size": 9058 },{ "source": "application/css/img/medalha_ouro.png", "type": "IMAGE", "size": 9092 },{ "source": "application/css/img/medalha_prata.png", "type": "IMAGE", "size": 8853 },{ "source": "application/css/img/smile1.png", "type": "IMAGE", "size": 4401 },{ "source": "application/css/img/smile2.png", "type": "IMAGE", "size": 4143 },{ "source": "application/css/img/ticks.png", "type": "IMAGE", "size": 3505 },{ "source": "application/css/img/trofeu_bronze.png", "type": "IMAGE", "size": 20295 },{ "source": "application/css/img/trofeu_ouro.png", "type": "IMAGE", "size": 19753 },{ "source": "application/css/img/trofeu_prata.png", "type": "IMAGE", "size": 17857 } ] }
